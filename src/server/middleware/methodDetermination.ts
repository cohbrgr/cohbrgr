import { NextFunction, Request, Response } from 'express';
import Logger from 'src/server/utils/logger';

export enum HttpMethod {
    GET = 'GET',
    HEAD = 'HEAD',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
    CONNECT = 'CONNECT',
    OPTIONS = 'OPTIONS',
    TRACE = 'TRACE',
    PATCH = 'PATCH',
}

const methodDetermination = (
    req: Request,
    res: Response,
    next: NextFunction,
): Response => {
    if (req.method !== HttpMethod.GET) {
        Logger.warn(
            `Unexpected Request with Method: ${req.method} on ${req.originalUrl}`,
        );
        res.statusCode = 405;
        return res.send('Method Not allowed');
    }
    next();
};

export default methodDetermination;
